# Communication between contracts on Optimism and Ethereum

This project is an application of optimism, an optimistic rollup scaling solution, it demonstrates how communication between layers works using Hardhat. It comes with a sample calculator contract, two special smart contracts [L1 contract that controls Calculator on L2](https://gitlab.com/Talan_Innovation_Factory/blockchain/optimism-communication-l1-l2/-/blob/optimism/contracts/FromL1_ControlL2Calculator.sol) and [L2 contract that controls Calculator on L1](https://gitlab.com/Talan_Innovation_Factory/blockchain/optimism-communication-l1-l2/-/blob/optimism/contracts/FromL2_ControlL1Calculator.sol).

Learn about more detailed steps in this link.

